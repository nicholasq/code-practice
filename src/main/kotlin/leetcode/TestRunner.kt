package leetcode

import kotlin.system.measureTimeMillis

class TestRunner<E, R>(
        var inputs: Array<E>,
        var outputs: Array<R>
) {

    class Builder<E>(private vararg val inputs: E) {

        fun <R> expectOutputs(vararg outputs: R): TestRunner<out E, out R> {
            return TestRunner(inputs, outputs)
        }

    }

    companion object {
        fun <E> usingInputs(vararg inputs: E): Builder<E> {
            return Builder(*inputs)
        }
    }

}

inline fun <E, R> TestRunner<out E, out R>.runTest(printValues: Boolean = false, crossinline f: (E) -> R): TestRunner<out E, out R> {

    val time = measureTimeMillis {

        for (i in 0 until inputs.size) {

            val input = inputs[i]
            val expected = outputs[i]
            if(printValues) {
                println("input:    $input")
                println("expected: $expected")
            }

            try {

                val result = f(input)

                if(printValues) { println("result:   $result") }

                if (result == expected) {
                    println("PASS")
                } else {
                    println("---- FAIL ----")
                    println("expected: $expected")
                    println("but got : $result")
                    println("--------------")
                }
            } catch (e: Exception) {

                println("---- FAIL ----")
                println("expected:   $expected")
                println("exception : ${e.message}")
                println("--------------")
            }
        }
    }

    println("Time taken: $time milliseconds")

    return this
}
