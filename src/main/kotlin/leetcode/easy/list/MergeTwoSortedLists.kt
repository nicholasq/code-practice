package leetcode.easy.list

import leetcode.TestRunner
import leetcode.runTest
import java.util.*

// problem: https://leetcode.com/problems/merge-two-sorted-lists
class MergeTwoSortedLists {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun mergeTwoLists(l1: ListNode?, l2: ListNode?): ListNode? {

            if(l1 == null) {
                return l2
            } else if(l2 == null) {
                return l1
            }

            var n1 = l1
            var n2 = l2
            val root = if(n1.`val` < n2.`val`) n1 else n2

            if(n1 == root) {
                n1 = n1.next
            }
            else {
                n2 = n2.next
            }

            var pointer: ListNode? = root

            while(n1 != null || n2 != null) {

                if(n1 == null) {
                    pointer?.next = n2
                    return root
                } else if(n2 == null) {
                    pointer?.next = n1
                    return root

                }

                if(n1.`val` < n2.`val`) {
                    pointer?.next = n1
                    pointer = pointer?.next
                    n1 = n1.next
                }
                else {
                    pointer?.next = n2
                    pointer = pointer?.next
                    n2 = n2.next
                }

            }
            return root
        }
    }

}

data class ListNode(var `val`: Int = 0) {

    var next: ListNode? = null
    override fun toString(): String {
        val sj = StringJoiner("->", "{", "}")
        var p :ListNode ? = this
        while (p != null) {
            sj.add("(${p.`val`})")
            p = p.next
        }
        return sj.toString()
    }

    override fun equals(other: Any?): Boolean {

        var a :ListNode ?= this
        var b :ListNode ?= other as ListNode?
        while(a != null && b != null) {

            if(a.`val` != b.`val`) {
               return false
            }
            a = a.next
            b = b.next
        }

        return a == null && b == null
    }

    override fun hashCode(): Int {
        return Objects.hashCode(`val`)
    }
}

fun makeLinkedList(vararg values: Int): ListNode {

    val root = ListNode(values[0])
    if (values.size == 1) return root
    var pointer: ListNode? = root
    for (i in 1 until values.size) {
        pointer?.next = ListNode(values[i])
        pointer = pointer?.next

    }

    return root
}

fun main(args: Array<String>) {

    val first = makeLinkedList(2, 5, 9)
    val second = makeLinkedList(1, 6, 8)
    val result = makeLinkedList(1, 2, 5, 6, 8, 9)

    val third = makeLinkedList(5)
    val fourth = makeLinkedList(1, 2, 4)
    val result2 = makeLinkedList(1, 2, 4, 5)

    TestRunner
            .usingInputs(Pair(first, second), Pair(third, fourth))
            .expectOutputs(result, result2)
            .runTest(printValues = true) { MergeTwoSortedLists.solution.mergeTwoLists(it.first, it.second) }
}

