package leetcode.easy.number

import leetcode.TestRunner
import leetcode.runTest
import java.util.*

// problem: https://leetcode.com/problems/roman-to-integer
class RomanToInteger {

    companion object {
        val solution = Solution()
        val values = mapOf(
                'I' to 1,
                'V' to 5,
                'X' to 10,
                'L' to 50,
                'C' to 100,
                'D' to 500,
                'M' to 1000
        )
    }

    class Solution {

        fun romanToInt(s: String): Int {

            val stack = Stack<Int>()
            var total = 0
            val lastIndex = s.length - 1

            for (c in 0..lastIndex) {

                val char = s[c]
                val value = values[char]!!

                if (stack.isNotEmpty()) {

                    val head = stack.peek()
                    if (head < value) {

                        val sum = stackSum(stack)
                        total += value - sum
                        stack.clear()
                    } else if (head > value) {

                        val sum = stackSum(stack)
                        total += sum
                        stack.clear()
                        stack.push(value)

                    } else {
                        stack.push(value)
                    }

                } else {
                    stack.push(value)
                }

            }

            return total + stackSum(stack)

        }

        private fun stackSum(stack: Stack<Int>): Int {
            if (stack.isEmpty()) return 0
            return stack.asSequence().reduce { a, b -> a + b }
        }
    }
}

fun main(args: Array<String>) {

    TestRunner
            .usingInputs("III", "IV", "IX", "LVIII", "MCMXCIV")
            .expectOutputs(3, 4, 9, 58, 1994)
            .runTest { RomanToInteger.solution.romanToInt(it) }
}