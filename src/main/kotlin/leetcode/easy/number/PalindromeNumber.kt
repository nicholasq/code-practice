package leetcode.easy.number

import leetcode.TestRunner
import leetcode.runTest

// problem: https://leetcode.com/problems/palindrome-number
class PalindromeNumber {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun isPalindrome(x: Int): Boolean {

            // If it's negative or ends in a zero
            if (x < 0 || (x % 10 == 0 && x != 0)) {
                return false
            }

            // If it's a single digit.
            if (x >= 0 && x < 10) {
                return true
            }

            var num = x
            var reverted = 0

            while (num > reverted) {

                reverted = reverted * 10 + (num % 10)
                num /= 10
            }

            return num == reverted || num == reverted / 10
        }
    }
}

fun main(args: Array<String>) {

    TestRunner
            .usingInputs(121, -121, 10, 12345432)
            .expectOutputs(true, false, false, false)
            .runTest { PalindromeNumber.solution.isPalindrome(it) }

}
