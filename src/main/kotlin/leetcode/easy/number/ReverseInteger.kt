package leetcode.easy.number

import leetcode.TestRunner
import leetcode.runTest

// problem: https://leetcode.com/problems/reverse-integer
class ReverseInteger {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun reverse(x: Int): Int {

            val negative = x < 0
            val asString = if (negative) "$x".substring(1) else "$x"
            val reversed = asString.reversed().toLong() * if (negative) -1 else 1
            if (reversed > Int.MAX_VALUE || reversed < Int.MIN_VALUE) return 0
            return reversed.toInt()
        }
    }
}

fun main(args: Array<String>) {

    TestRunner
            .usingInputs(123, 12345, 54321, 2_123_456_789, -123, -2147483648)
            .expectOutputs(321, 54321, 12345, 0, -321, 0)
            .runTest { ReverseInteger.solution.reverse(it) }

}