package leetcode.easy.array

import leetcode.TestRunner
import leetcode.runTest
import java.util.*

//problem: https://leetcode.com/problems/remove-duplicates-from-sorted-array
class RemoveDuplicatesSortedArray {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun removeDuplicates(nums: IntArray): Int {

            var pointer = 0

            for (i in 1 until nums.size) {

                if (nums[pointer] != nums[i]) {
                    nums[pointer + 1] = nums[i]
                    pointer += 1
                }
            }

            return pointer + 1
        }
    }
}

fun main(args: Array<String>) {

    val input = Pair(intArrayOf(0, 0, 1, 1, 1, 2, 2, 3, 3, 4), intArrayOf(0, 1, 2, 3, 4))

    TestRunner
            .usingInputs(input)
            .expectOutputs(5)
            .runTest(true) {

                val result = RemoveDuplicatesSortedArray.solution.removeDuplicates(it.first)
                println(it.first)
                println(it.second)
                // Have to check that the array that was passed in
                // was modified in place and duplicates removed.
                for (i in 0 until it.second.size) {
                    if (it.first[i] != it.second[i]) {
                        throw RuntimeException("The array did not have duplicates removed")
                    }
                }

                // But the solution has to return the new "size" of the modified array
                return@runTest result
            }
}