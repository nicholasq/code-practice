package leetcode.easy.array

import leetcode.TestRunner
import leetcode.runTest

//problem: https://leetcode.com/problems/two-sum
class TwoSum {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun twoSum(nums: IntArray, target: Int): IntArray {

            val diffMap = nums.mapIndexed { index, i -> target - i to index }.toMap()

            nums.forEach {
                println("nums[$it] = ${nums[it]} ; so 9 - ${nums[it]} = ${9 - nums[it]}. this becomes entry in map ${9 - nums[it]} -> $it")
            }


            for (i in 0 until nums.size) {

                val diffIndex = diffMap[nums[i]]
                if (diffIndex != null && diffIndex != i) {
                    return intArrayOf(i, diffIndex)
                }
            }

            return intArrayOf()

        }
    }
}

fun main(args: Array<String>) {


    TestRunner
            .usingInputs(Pair(intArrayOf(2, 7, 11, 15), 9))
            .expectOutputs(9)
            .runTest { TwoSum.solution.twoSum(it.first, it.second) }

}