package leetcode.easy.array

import leetcode.TestRunner
import leetcode.runTest

//problem: https://leetcode.com/problems/remove-element
class RemoveElement {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun removeElement(nums: IntArray, `val`: Int): Int {

            var pointer = 0

            for (i in 0 until nums.size) {

                if (nums[i] != `val`) {
                    nums[pointer] = nums[i]
                    pointer++
                }
            }

            return pointer
        }
    }
}

fun main(args: Array<String>) {

    val input = Triple(intArrayOf(3, 2, 2, 3), 3, intArrayOf(2, 2))

    TestRunner
            .usingInputs(input)
            .expectOutputs(2)
            .runTest {

                val result = RemoveElement.solution.removeElement(it.first, it.second)
                println(it.first)
                println(it.second)
                // Have to check that the array that was passed in
                // was modified in place and duplicates removed.
                for (i in 0 until it.third.size) {
                    if (it.first[i] != it.third[i]) {
                        throw RuntimeException("The array did not have duplicates removed")
                    }
                }

                // But the solution has to return the new "size" of the modified array
                return@runTest result
            }
}