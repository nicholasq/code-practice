package leetcode.easy.string

import leetcode.TestRunner
import leetcode.runTest

// problem: https://leetcode.com/problems/longest-common-prefix/
class LongestCommonPrefix {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun longestCommonPrefix(strs: Array<String>): String {

            if (strs.isEmpty()) {
                return ""
            }

            if (strs.size == 1) {
                return strs[0]
            }

            var smallestLen = Int.MAX_VALUE
            var smallestStr = ""

            for (str in strs) {

                if (str.isEmpty()) {
                    return ""
                }
                if (str.length < smallestLen) {
                    smallestLen = str.length
                    smallestStr = str
                }


            }

            for (i in smallestLen.downTo(0)) {

                val sub = smallestStr.substring(0, i)
                var inAll = true

                for (str in strs) {

                    if (!str.startsWith(sub)) {
                        inAll = false
                    }
                }

                if (inAll) {
                    return sub
                }
            }

            return ""
        }
    }
}

fun main(args: Array<String>) {

    TestRunner
            .usingInputs(
                    arrayOf("flower", "flow", "flight"),
                    arrayOf("flowing", "flow", "flower", "flowed"),
                    arrayOf("dog", "racecar", "car")
            )
            .expectOutputs("fl", "flow", "")
            .runTest { LongestCommonPrefix.solution.longestCommonPrefix(it) }
}