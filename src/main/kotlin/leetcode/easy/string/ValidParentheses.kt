package leetcode.easy.string

import leetcode.TestRunner
import leetcode.runTest
import java.util.*

// problem: https://leetcode.com/problems/valid-parentheses/
class ValidParentheses {

    companion object {
        val solution = Solution()
    }

    class Solution {

        fun isValid(s: String): Boolean {

            if (s.length == 1) return false
            val stack = Stack<Char>()
            for (c in s) {
                when (c) {
                    '{' -> stack.push(c)
                    '[' -> stack.push(c)
                    '(' -> stack.push(c)
                    else -> {
                        if (stack.isEmpty()) return false
                        val head = stack.pop()
                        if (c == '}' && head != '{') return false
                        if (c == ']' && head != '[') return false
                        if (c == ')' && head != '(') return false
                    }
                }
            }

            return stack.isEmpty()
        }
    }
}

fun main(args: Array<String>) {

    TestRunner
            .usingInputs("()", "()[]{}", "(]", "([)]", "{[]}", "", "[[")
            .expectOutputs(true, true, false, false, true, true, false)
            .runTest { ValidParentheses.solution.isValid(it) }
}